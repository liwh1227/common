module gitee.com/liwh1227/common

go 1.16

require (
	github.com/gin-gonic/gin v1.8.1
	github.com/lestrrat-go/strftime v1.0.6
	github.com/pkg/errors v0.9.1
	go.uber.org/zap v1.21.0
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.3.5
	gorm.io/gorm v1.23.8
)
